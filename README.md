### Dependencies Required

NOTE: spring-cloud-starter-netflix is deprecated
```java
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-hystrix</artifactId>
        <version>2.1.2.RELEASE</version>
    </dependency>
```

---

### Implementation

##### Step 1

Simply add the `@HystrixCommand(fallbackmethod='nameOfFallBackMethod')` to the desired method like so:
```jave
    @HystrixCommand(fallbackMethod = "defaultLocalizationLanguagePack")
    public String getLocalizationLanguagePack(String appVersion){
        return new RestTemplate().getForObject("http://localhost:8080/v1/localization/language-pack?appVersion={appVersion}", String.class, appVersion);
    }
```

Then created a fallback method with the name stated in @HystrixCommand with the exact same parameter, even if these parameters are not used. Example
```jave
    public String defaultLocalizationLanguagePack(String appVersion){
        return "{'appVersion':'1', 'content':'This is the fallback method'}";
    }
```
NOTE: the return value can be pull from cache if such mechanism is implemented

##### Step 2

Just add `@EnableCircuitBreaker` to your Application class.

---
### Hystrix Dashboard

To get visualization on the health and performance of the methods called, include these dependencies
```jave
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-hystrix-dashboard</artifactId>
        <version>2.1.2.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
        <version>2.1.7.RELEASE</version>
    </dependency>
```

Then follow these steps:

1.  include `management.endpoints.web.exposure.include=hystrix.stream` in .properties or .yaml file
2. visit localhost:8080/hyxtrix
3. key in --> `http://localhost:8080/actuator/hystrix.stream`

And you will get your Dashboard!


