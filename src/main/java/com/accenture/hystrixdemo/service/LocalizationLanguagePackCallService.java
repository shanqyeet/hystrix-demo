package com.accenture.hystrixdemo.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class LocalizationLanguagePackCallService {

    @HystrixCommand(fallbackMethod = "defaultLocalizationLanguagePack")
    public String getLocalizationLanguagePack(String appVersion){
        return new RestTemplate().getForObject("http://localhost:8080/v1/localization/language-pack?appVersion={appVersion}", String.class, appVersion);
    }


    // fallback method must have exact match to the original method regardless the parameter are consumed or not
    public String defaultLocalizationLanguagePack(String appVersion){
        return "{'appVersion':'1', 'content':'This is the fallback method'}";
    }
}
