package com.accenture.hystrixdemo.controller;

import com.accenture.hystrixdemo.service.LocalizationLanguagePackCallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LocalizationCallController {

    @Autowired
    private LocalizationLanguagePackCallService localizationLanguagePackCallService;

    @GetMapping(value="/localization/get-language-pack", produces="application/json")
    public String getLocalizationLanguagePack(@RequestParam("appVersion") String appVersion){
        return localizationLanguagePackCallService.getLocalizationLanguagePack(appVersion);
    }
}
